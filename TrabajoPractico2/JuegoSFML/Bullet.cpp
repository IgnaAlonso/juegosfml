#include "Bullet.h"

const float degree2radian = (3.14159f / 180.0f);

namespace game
{
	Bullet::Bullet(sf::Vector2f& initial_position, float movement_angle)
		: movement(std::cosf(movement_angle * degree2radian), std::sinf(movement_angle * degree2radian))
		, liveact(live) 
		, isalive(true)
	{
		setPosition(initial_position);
	}

	void Bullet::update(float delta_time_seconds)
	{
		if (!isalive) return;

		liveact -= delta_time_seconds;

		if (liveact < 0) isalive = false;

		sf::Vector2f velocity = movement * speed * delta_time_seconds;
		move(velocity);

		sf::Vector2f position = getPosition();
		if (position.x <= -1.0f) position.x = 640.0f + 1.0f;
		else if (position.x >= 641.0f) position.x = -1.0f;

		if (position.y <= -1.0f) position.y = 480.0f + 1.0f;
		else if (position.y >= 481.0f) position.y = -1.0f;

		setPosition(position);
	}

	void Bullet::draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		sf::Texture texture;

		texture.loadFromFile("Images/Bullet.png");

		sf::Sprite sprite(texture);

		states.transform *= getTransform();

		target.draw(sprite, states);
	}

	bool Bullet::isAlive()
	{
		return isalive;
	}

	void Bullet::kill()
	{
		isalive = false;
	}

}