#pragma once

#include <SFML/Graphics.hpp>

const float velMove = 1450.0f; 

namespace game
{
	class Player : public sf::Sprite
	{
	public:
		Player();
		~Player();

		void reset();

		void update(float delta_time_seconds);

		void draw(sf::RenderTarget &target, sf::RenderStates states) const;

		sf::Texture texture;

		sf::Sprite sprite;

		sf::Sprite PlayerSprite();

	protected:
		sf::Vector2f velocity; 
	};
};
