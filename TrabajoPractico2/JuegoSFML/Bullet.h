#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

const float live = 4.0f;
const float speed = 70.0f;
const float shoot_speed = 0.5f;

namespace game
{
	class Bullet : public sf::Sprite
	{
	public:
		Bullet(sf::Vector2f& initial_position, float movement_angle);

		void update(float delta_time_seconds);

		void draw(sf::RenderTarget &target, sf::RenderStates states) const;

		bool isAlive();

		void kill();

	protected:
		sf::Vector2f movement;

		float liveact;

		bool isalive;
	};
}
