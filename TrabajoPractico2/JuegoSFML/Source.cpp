#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Bullet.h"
#include "Enemy.h"

#ifdef DEBUG
#include "vld.h"
#endif

#include <list>
#include <iostream>

using namespace std;

typedef	std::list<game::Bullet*> BulletList;
typedef std::list<game::Bullet*>::iterator BulletIndex;


typedef std::list<game::Enemy*> EnemyList;
typedef std::list<game::Enemy*>::iterator EnemyIndex;

int main()
{
	sf::RenderWindow window(sf::VideoMode(640, 480), "SFML Juego Alonso");

	sf::Clock syncronice_timer;

	game::Player player;

	BulletList bullets;
	float time_to_next_bullet = 0.0f;

	EnemyList enemys;

	bool need_reset_game = true;

	while (window.isOpen())
	{
		//Resetear
		if (need_reset_game)
		{
			need_reset_game = false;

			player.reset();

			BulletIndex I = bullets.begin();
			BulletIndex E = bullets.end();
			while (I != E)
			{
				game::Bullet* bullet = (*I);
				delete bullet;
				++I;
			};
			bullets.clear();

			EnemyIndex EI = enemys.begin();
			EnemyIndex EE = enemys.end();
			while (EI != EE)
			{
				game::Enemy* enemy = (*EI);
				delete enemy;
				++EI;
			};
			enemys.clear();

			//Agregar enemigos
			game::Enemy* newenemy = new game::Enemy(sf::Vector2f(std::rand() % 640, std::rand() % 480), std::rand() % 360);
			game::Enemy* newenemy2 = new game::Enemy(sf::Vector2f(std::rand() % 640, std::rand() % 480), std::rand() % 360);
			game::Enemy* newenemy3 = new game::Enemy(sf::Vector2f(std::rand() % 640, std::rand() % 480), std::rand() % 360);
			game::Enemy* newenemy4 = new game::Enemy(sf::Vector2f(std::rand() % 640, std::rand() % 480), std::rand() % 360);

			enemys.push_back(newenemy);
			enemys.push_back(newenemy2);
			enemys.push_back(newenemy3);
			enemys.push_back(newenemy4);
		}

		float delta_time_seconds = syncronice_timer.restart().asSeconds();

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			{
				window.close();
			}
		}

		//COLISIONES
		
		std::vector<game::Enemy*> addedEnemys;

		//BALAS-ENEMIGOS
		EnemyIndex EI0 = enemys.begin();
		EnemyIndex EE0 = enemys.end();
		while (EI0 != EE0)
		{
			game::Enemy* enemy = (*EI0);

			BulletIndex I = bullets.begin();
			BulletIndex E = bullets.end();
			while (I != E)
			{
				game::Bullet* bullet = (*I);

				if (!bullet->isAlive())
				{
					++I;
					continue;
				}

				if (enemy->getGlobalBounds().intersects(bullet->getGlobalBounds()))
				{
					bullet->kill();

					enemy->incrementEnemyLevel();

					if (enemy->isAlive())
					{
						game::Enemy* newEnemy = new game::Enemy(sf::Vector2f(enemy->getPosition().x, enemy->getPosition().y), std::rand() % 360, enemy->getEnemyLevel());
						addedEnemys.push_back(newEnemy);
					}

					break;
				}
				++I;
			}
			++EI0;
		}

		enemys.insert(enemys.end(), addedEnemys.begin(), addedEnemys.end());

		//JUGADOR-ENEMIGOS
		EI0 = enemys.begin();
		EE0 = enemys.end();
		while (EI0 != EE0)
		{
			game::Enemy* enemy = (*EI0);

			if (enemy->EnemySprite().getGlobalBounds().intersects(player.PlayerSprite().getGlobalBounds()))
			{
				std::cout << "Holi" << std::endl;
				//need_reset_game = true;
				//break;
			}

			++EI0;
		}
		
		//DISPARAR
		time_to_next_bullet -= delta_time_seconds;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && time_to_next_bullet < 0.0f)
		{
			time_to_next_bullet = shoot_speed;

			sf::Vector2f position = player.getPosition();
			float movement_angle = player.getRotation();

			position += 10.0f * sf::Vector2f(std::cosf(movement_angle * (3.14159f / 180.0f)), std::sinf(movement_angle * (3.14159f / 180.0f)));

			game::Bullet* newBullet = new game::Bullet(position, movement_angle);
			if (newBullet) bullets.push_back(newBullet);
		}
		
		player.update(delta_time_seconds);

		//UPDATE BALAS
		BulletIndex I = bullets.begin();
		BulletIndex E = bullets.end();
		while (I != E)
		{
			game::Bullet* bullet = (*I);

			if (bullet->isAlive())
			{
				bullet->update(delta_time_seconds);
				++I;
			}
			else
			{
				delete bullet;
				I = bullets.erase(I);
			}
		}

		//UPDATE ENEMIGOS
		EnemyIndex EI = enemys.begin();
		EnemyIndex EE = enemys.end();
			while (EI != EE)
			{
				game::Enemy* enemy = (*EI);

				if (enemy->isAlive())
				{
					enemy->update(delta_time_seconds);
					++EI;
				}
				else
				{
					delete enemy;
					EI = enemys.erase(EI);
				}
			}

		window.clear();

		window.draw(player);

		//DIBUJO LAS BALAS
		I = bullets.begin();
		E = bullets.end();
		while (I != E)
		{
			game::Bullet* bullet = (*I);

			window.draw(*bullet);
			++I;
		}

		//DIBUJO LOS ENEMIGOS
		EI = enemys.begin();
		EE = enemys.end();
		while (EI != EE)
		{
			game::Enemy* enemy = (*EI);
			window.draw(*enemy);
			++EI;
		}

		window.display();

		while (syncronice_timer.getElapsedTime().asSeconds() < 1.0f / 30.0f)
			sf::sleep(sf::seconds(1.0f / 60.0f));
	}

	return 0;
}