#pragma once

#include <SFML/Graphics.hpp>

const float enemy_speed[3] = { 40.0f, 60.0f, 80.0f };
const float enemy_radius[3] = { 30.0f, 15.0f, 7.0f };

namespace game
{
	class Enemy :public sf::Sprite
	{
	public:

		sf::Texture texture;

		sf::Sprite sprite;

		Enemy(sf::Vector2f& initial_position, float movement_angle, sf::Uint8 rock_level = 0);

		void update(float delta_time_seconds);

		void draw(sf::RenderTarget &target, sf::RenderStates states) const;

		bool isAlive();

		sf::Uint8 getEnemyLevel();

		void incrementEnemyLevel();

		sf::Sprite EnemySprite();


	protected:
		sf::Uint8 enemy_level;

		sf::CircleShape shape;

		sf::Vector2f enemydirection;

		bool isalive;

	};
}
