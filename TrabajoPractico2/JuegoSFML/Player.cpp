#include "Player.h" //Inclusi�n de las definici�n de la clase nave

namespace game
{
	Player::Player()
	{
		reset();
	}

	Player::~Player()
	{
	}

	void Player::reset()
	{
		setPosition(320.0f, 240.0f);
		setRotation(0.0f);  
		velocity.x = 0.0f;
		velocity.y = 0.0f;

		texture.loadFromFile("Images/Player.png");

		sprite.setTexture(texture);

		sprite.setScale(2, 2);

		setOrigin(texture.getSize().x / 2, texture.getSize().y / 2);
	}

	void Player::update(float delta_time_seconds)
	{

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			rotate((-90) * delta_time_seconds);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			rotate(90 * delta_time_seconds);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			float rotation = getRotation();

			float cosRotation = std::cosf(rotation * (3.14159f / 180.0f));
			float sinRotation = std::sinf(rotation * (3.14159f / 180.0f));

			velocity.x = velMove * delta_time_seconds * cosRotation;
			velocity.y = velMove * delta_time_seconds * sinRotation;

			move(velocity * delta_time_seconds);
		}

			sf::Vector2f position = getPosition();

			if (position.x <= -10.0f) position.x = 640.0f + 10.0f;
			else if (position.x >= 650.0f) position.x = -10.0f;

			if (position.y <= -10.0f) position.y = 480.0f + 10.0f;
			else if (position.y >= 490.0f) position.y = -10.0f;

			setPosition(position);
	}

	void Player::draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		//sf::Sprite sprite(texture);

		//sprite.setScale(2, 2);
		
		states.transform *= getTransform();

		target.draw(sprite, states);
	}

	sf::Sprite Player::PlayerSprite()
	{
		return sprite;
	}

};
